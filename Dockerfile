# Based on dockerhub fellah/gitbook-ebook
#
# We keep our own in gitlab-registry.cern.ch/cloud, just in case.

FROM cern/cc7-base

MAINTAINER Alvaro Gonzalez <Alvaro.Gonzalez@cern.ch>

RUN yum install -y npm git && \
    npm install gitbook-cli -g && \
    npm cache clear && \
    gitbook fetch && \
    sed -i 's/confirm: true/confirm: false/g' ./root/.gitbook/versions/3.2.3/lib/output/website/copyPluginAssets.js

